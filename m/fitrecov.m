function [Pout,R,Rf,S]=fitrecov(Data,Par,options)
% [Pout,R,Rf,S]=fitrecov(Data,Pin,options)
%
% Fit multiple recovery peaks to isochronal recovery data.
% Stepwise annealing is assumed.
%
% Input arguments
%   Data : [N x 3] data matrix
%           1st column: temperature T (K)
%           2nd column: annealing time dt (s)
%           3rd column: recovery f (% or other units)
%   Par  : [L x 5] input parameter matrix for L peaks (each row is 1 peak)
%           1st & 2nd column: (T1,T2) range to search for the peak max.
%           3rd & 4th column: range for log10(ko), logarithm of the
%           pro-exponential factor
%           5th column: reaction code m
%             m=0 : correlated recovery - delta function distribution
%             m=0.5 : correlated recovery - mod. gaussian distribution
%             m=1 : 1st order
%             m>1 : m-th order
%   options : [optional] options structure
%             Available options:
%             options.testing = {0} | 1. If set plotting is on during
%                                        fitting and some messages are
%                                        displayed
%             options.diff    = {'dT'} | 'dlogT' | 'dt'
%                               Select how to do the differentiation,
%                               df/dT, df/dlogT, df/dt
%
% Output arguments
%   Pout : [L x 4] output parameter matrix for L peaks (each row is 1 peak)
%           1st column: Tmax peak temperature
%           2nd column: A total peak recovery
%           3rd column: ko, pre-exponential factor
%           4th column: Ea, activation energy [e^(-Ea/kT)]
%    R   : Nx1, experimental differential spectrum
%    Rf  : Nx1, fitted differential spectrum
%    S   : NxL, fitted individual peaks, scaled
%


% if no options are given,
% set defaults
if nargin<3 || isempty(options),
  options.testing = 0;
  options.diff = 'dT';
end
if ~isfield(options,'testing'),
    options.testing = 0;
end
if ~isfield(options,'diff'),
    options.diff = 'dT';
end

% OCTAVE needs packages optim, specfun
isOctave = exist('OCTAVE_VERSION', 'builtin') ~= 0;
if isOctave, 
  pkg load optim
  pkg load specfun
end

% get the input data
N=size(Data,1); % # of data points
T = Data(:,1);
dt = Data(:,2);
F = Data(:,3);

% check that T is monotonously increasing
if any(diff(T)<=0),
  error('T must be monotonously increasing');
  return
end

% define the differentiation vector dx
if strcmp(options.diff,'dT')
  dx = diff(T);
elseif strcmp(options.diff,'dlogT')
  dx = diff(log(T));
elseif strcmp(options.diff,'dt')
  dx = dt(2:end);
else
  error("Invalid differentiation option");
end

% calculate the data derivative
R = -diff(F)./dx;

% # of peaks
L = size(Par,1);
% reaction order for each peak
m = Par(:,5);

% Create initial fitting parameter vector
% 2 parameters for each peak : Tmax and log10(ko)
npar = 2*L;
p = zeros(1,npar);
% The first L values are Tmax
% set initial Tmax at the center of (T1,T2)
p(1:L) = mean(Par(:,1:2)');
% The next L are log10(ko)
% set initial log10(ko) at the center of given range
p(L+1:2*L) = mean(Par(:,3:4)');

% set upper - lower bounds for fitting parameters
ub=zeros(1,npar);
lb=zeros(1,npar);
% bounds for Tmax
lb(1:L) = Par(:,1)';
ub(1:L) = Par(:,2)';
% bounds for log10(ko)
lb(L+1:2*L) = Par(:,3)';
ub(L+1:2*L) = Par(:,4)';

% prepare a vector to recieve values of Eo
Eo = zeros(L,1);
% prepare a vector to recieve the peak area values
A = zeros(L,1);
% prepare a matrix for the individual peaks
S = zeros(N-1,L);
% prepare a vector for the fitted spectrum = sum of peaks
Rf = zeros(N-1,1);
% prepare a vector for the fitted Tm
To = zeros(L,1);
% prepare a vector for the fitted ko
ko = zeros(L,1);

% prepare the non-linear least squares  options
if options.testing,
    lsq_options = optimset('Display','iter');
    hfunc = @chi2_testing;
else
    lsq_options = optimset('Display','iter');
    hfunc = @chi2;
end
if isfield(options,'TolFun'),
    lsq_options.TolFun = options.TolFun;
end
if isfield(options,'TolX'),
    lsq_options.TolX = options.TolX;
end

% perform the non-linear least squares
% and obtain fitting parameter vector p
p = lsqnonlin(hfunc,p,lb,ub,lsq_options);

% calculate the model peaks based on the final result
% This also calculates Tm,ko,A,S
calc_model(p);
% get the total fitted R
Rf = S*A;
% scale the peaks according to area
S = S.*(ones(N-1,1)*A');
% correct Tmax
Tm = To;
for i=1:L,
  Tm(i) = corrTmax(To(i),Eo(i),m(i));
end

% pack all parameters in Pout
kb = 8.617e-5;
Pout = [Tm A ko Eo*kb];

% left-pad diff vectors with 0
R = [0; R];
Rf = [0; Rf];
S = [zeros(1,L); S];

%%% Function Definitions %%%

  % Calculate the vector of differences (data - model)
  % given the parameters par = [Tmax,ko]
  function df=chi2(par)
      df = calc_model(par);
  end

  % Same as chi2 but also plot the data & model
  function df=chi2_testing(par)
      df = chi2(par);
      Rf = S*A;
      S = S.*(ones(N-1,1)*A');
      plot(T(2:end),R,'.',T(2:end),Rf,T(2:end),S);
      drawnow
  end

  % Calculate the model function = sum of recovery peaks
  % and return residual
  function df = calc_model(par)
      % get the parameters from par
      To = par(1:L);
      ko = 10.^(par(L+1:2*L));
      % compute the peak shapes
      for i=1:L,
        [n, Eo(i)] = calc_n(T,dt,To(i),ko(i));
        n = rec_func(n,m(i));
        S(:,i) = -diff(n)./dx;
      end
      % obtain A from non-negative least squares
      % df is the difference R - S*A
      [A,~,df] = lsqnonneg(S,R);
  end

end % fit_rec_peaks

function f = rec_func(n,m)
%
% Calculate the recovery law
%
% Input:
% n : [N x 1] vector, # of events
% m : order or type of reaction
%    m=0 : correlated recovery - delta function distribution
%    m=0.5 : correlated recovery - mod. gaussian distribution
%    m=1 : 1st order
%    m>1 : m-th order
%
% Output
%  f : [N x 1] fractional recovery. f goes from f(0)=1 to f(inf)=0 

% compute recovery function f
  switch m,
    case 0  % correlated recovery - delta func
        f = 1-erfc(sqrt(1./n/2));
    case 0.5, % modified exp correlated recovery
        n = 0.674573519*n;
        n2=sqrt(n);
        f = exp(n).*erfc(n2);
        i=find(n>=700);
        f(i) = 1/sqrt(pi)./n2(i);
    case 1, % 1st order process
        f = exp(-n);
    otherwise % m-th order process
        f = (1+(m-1)*n).^(1/(1-m));
  end

end

function Tc = corrTmax(To,E,m)

  switch m,
      case 0  % correlated recovery - delta func
          Tc = To*(1-2*(To/E)^2*2);
      case 0.5, % modified exp correlated recovery
          Tc = To*(1-2*(To/E)^2*6.146);
      otherwise % m-th order process
          Tc = To*(1-2*(To/E)^2*m);
  end

end

function [n,E] = calc_n(T,dt,To,ko)
%
% Calculate the number of events n due to a thermally
% activated mechanism ko*exp(-E/kT), where ko
% is the rate constant and E the activation energy, in an
% annealing experiment where T are the temperatures and dt annealing times.
%
% As input it is given To, the temperature where n ~ 1
%
% Input:
% T : [N x 1] vector of annealing temperatures (K)
%     T must be monotonously increasing
% dt : [N x 1] vector of annealing time per temperature (s)
% To : Temperature at n ~ 1
% ko : Rate const (s^-1)
% m : order or type of reaction
%    m=0 : correlated recovery - delta function distribution
%    m=0.5 : correlated recovery - mod. gaussian distribution
%    m=1 : 1st order
%    m>1 : m-th order
%
% Output
%  n : [N x 1] # of events
%  E : estimated activation energy (eV)
%

% find a=dT/dt close to To
i = lookup (T, To, 'lr');
alpha = (T(i+1)-T(i))/dt(i+1);

% find the activation energy (in K)
E = To*lambertw(ko*To/alpha);

% calc n
n = ko*cumsum(exp(-E./T).*dt);

end



