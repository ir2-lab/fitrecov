%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% FITRECOV - Test1
%
% Create a simulated spectrum and fit it
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear

addpath('../m')

% set-up temperatures & time for annealing
a = 1;
dT = 2;
T=(80:dT:180)';
dt=ones(size(T))*(dT/a);

% simulate 2 correlated recovery peaks
ko=1e13;
E = [0.3 0.35];
kb = 8.617e-5;
x = 2*ko*cumsum(exp(-E(1)./T/kb).*dt);
f1 = 1-erfc(sqrt(1./x));
x = 2*ko*cumsum(exp(-E(2)./T/kb).*dt);
f2 = 1-erfc(sqrt(1./x));

f = f1 + 2*f2;

% normalize
f = f/f(1);

% add gaussian measurement noise
% pkg load statistics
%f = f + normrnd(zeros(size(f)),0.001);
f = f + rand(size(f))*0.005;

%% Fit the simulated spectrum
Par = [ ...
      90 120    11   15  0
     120 140    11   15  0
     ];

options.testing = 0;
options.diff = 'dT';

[Pout,R,Rf,S]=fitrecov([T dt f],Par,options);
num2str(Pout,3)

% create plot
figure 1
clf
clrs = get(gca,'colororder');

hold on
color = clrs(1,:);
alpha = 1/4;
h = fill([T; flipud(T)],[S(:,1); zeros(size(T))],color,...
  'EdgeColor',color,...
  'EdgeAlpha',alpha,...
  'FaceAlpha',alpha);
color = clrs(2,:);
h = fill([T; flipud(T)],[S(:,2); zeros(size(T))],color,...
  'EdgeColor',color,...
  'EdgeAlpha',alpha,...
  'FaceAlpha',alpha);
plot(T,zeros(size(T)),'k')

hp2 = plot(T,Rf,'-;fit;','color',clrs(2,:),'linewidth',2);
hp1 = plot(T,R,'o;data;','color',clrs(1,:),...
  'MarkerFaceColor','w','MarkerSize',5);
box on
hold off
hl = legend([hp1, hp2]);
ylabel('-df/dT (%/K)')
xlabel('T (K)')
title('Fit to simulated data')
ylim([-0.005 0.05])

##print -dpdfcrop -svgconvert test1

