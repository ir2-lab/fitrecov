%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% FITRECOV - plot recovery functions and their derivative
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear

% OCTAVE needs package specfun for lambertw
isOctave = exist('OCTAVE_VERSION', 'builtin') ~= 0;
if isOctave, 
  pkg load specfun
end

n = logspace(-6,3.4,101)';

ko = 1e5; 
a = 0.01;
E = 0.3/8.617e-5;
q = ko*E/4/a;
T0 = 0.5*E/lambertw(sqrt(q));
T = 0.5*E./lambertw(sqrt(q./n));

q = 1 - 2*(T0/E)^2*[1 2 2 6.145];

b = 0.674573519;

f(:,1) = exp(-n);
f(:,2) = 1./(1+n);
f(:,3) = 1-erfc(1./sqrt(2*n));
f(:,4) = exp(b*n).*erfc(sqrt(b*n));
i = find(b*n>700);
f(i,4) = 1/sqrt(pi)./sqrt(b*n(i));

figure 1
clf
plot(T/T0,f,[1 1],[0 1],'k')

legend('m = 1','m = 2','m = 0','m = 1/2',...
  'location','southwest')
xlabel('T / T_0')
ylabel('f')
text(1.2,0.7,{'k_0 = 10^5 s^{-1}',...
        '\alpha = 0.01 K/s',...
        'E = 0.3 eV',...
        'T_0 = 190 K'})
  
##print2pdf(gcf,[12 9],'f_vs_T')

figure 2
clf
t1 = 0.5*(T(1:end-1)+T(2:end))/T0;
dfdt = -diff(f)./diff(T)*T0;
plot(t1,dfdt) 
hold on
plot([1 1],[0 1]*8,'k')
plot((q'*ones(1,2))',...
  (max(dfdt)'*ones(1,2) + 0.2*[1 -1])','k')
hold off
ylim([0 8])

legend('m = 1','m = 2','m = 0','m = 1/2',...
  'location','northeast')
xlabel('T / T_0')
ylabel('-T_0 df / dT')
text(0.5,6,{'k_0 = 10^5 s^{-1}',...
        '\alpha = 0.01 K/s',...
        'E = 0.3 eV',...
        'T_0 = 190 K'})


##print2pdf(gcf,[12 9],'df_vs_T')