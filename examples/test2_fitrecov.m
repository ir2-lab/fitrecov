%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% FITRECOV - Test2
%
% Fit experimental data
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear

A=load('Irrad9_RvsTa_raw.dat');
T = A(:,1);
dt = A(:,2);
DR = A(:,3);
i=find(T<180);
T=T(i);
dt=dt(i);
DR=DR(i);
f = 100 + cumsum(DR);

%% Fit the pure Fe spectrum
Par = [ ...
     50    57    5   13  1
     57    65    8   13  1
     65    75    8   13  1
     87    95    8   13  1
     100   120   8   13  0.5
     120   130   8   13  1.5
     160   170   8   13   2
     ];

options.testing = 0;
options.diff = 'dT';

% no options = no testing, diff with T
[Pout,R,Rf,S]=fitrecov([T dt f],Par,options);
num2str(Pout,3)

% plot figure
figure 1
clf

clrs = get(gca,'colororder');
clrs = [clrs; clrs];
hold on
alpha = 1/4;
for i=1:size(S,2),
  color = clrs(i,:);
  h = fill([T; flipud(T)],[S(:,i); zeros(size(T))],color);
  set(h,'FaceColor',color,'EdgeColor',color,'FaceAlpha',...
      alpha,'EdgeAlpha',alpha);
end
plot(T,zeros(size(T)),'k')
hp2 = plot(T,Rf,'-;fit;','color',clrs(2,:),'linewidth',2);
hp1 = plot(T,R,'o;data;','color',clrs(1,:),...
  'MarkerFaceColor','w','MarkerSize',5);
box on
hold off
xlim([33 175])
ylim([-0.1 2.5])
hl = legend([hp1, hp2]);
ylabel('-df/dT (%/K)')
xlabel('T (K)')
title('Resistivity recovery of Fe')

##print -dpdfcrop -svgconvert test2
