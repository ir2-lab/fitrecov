\documentclass[12pt,a4paper,notitlepage]{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage[left=2cm,right=2cm,top=2cm,bottom=2cm]{geometry}
\author{GA}
\title{\texttt{fitrecov.m}: Fitting recovery spectra in \texttt{MATLAB}}

% bib style
\usepackage[round]{natbib}
\usepackage{hyperref}

\begin{document}

\maketitle

\section{Introduction}
This document describes a procedure for fitting recovery rate spectra, as those occurring in  resistivity recovery experiments after irradiation, quenching or mechanical 
deformation of metallic materials. A spectrum is decomposed into a set of recovery peaks, each one representing a thermally activated process. 

The numerical fitting procedure has been implemented in \texttt{MATLAB}/\texttt{OCTAVE}.

\section{Statement of the problem}

In a typical recovery experiment an initial number of defects $N_0$ is generated in the material usually at low temperature $T_0$. The material is then annealed at successively increasing temperatures $T_0 < T_1 < T_2 < ... $ for time intervals $ \Delta t_1, \Delta t_2, ...$. At the end of each annealing step a property is measured which is sensitive to the number of defects. In such a way the evolution of defect number $N_1, N_2, ...$ during the annealing is obtained. The fractional recovery $f_i$ is defined as
\begin{equation}
f_i=N_i/N_0, \quad i=1,2,...
\end{equation} 
and goes from the initial value $f=1$ towards $f=0$.
 
A more informative quantity is the recovery rate $R$ defined either as
\begin{equation}
R_i = -\frac{\Delta f_i}{\Delta t_i}=-\frac{f_i - f_{i-1}}{\Delta t_i}
\end{equation}
in units of s$^{-1}$ or as
\begin{equation}
R_i = -\frac{\Delta f_i}{\Delta T_i}=-\frac{f_i - f_{i-1}}{T_i - T_{i-1}}
\end{equation}
in units of K$^{-1}$. The minus sign is inserted so that $r_i$ is positive when $f$ is decreasing.

In situations where the annealing temperature is varied continuously, a temperature ramp being the most typical example, the recovery and the recovery rate become continuous functions of temperature
\begin{equation}
f=f(T), \quad R(T) = -\frac{df}{dT}= -\frac{df}{dt}\frac{dt}{dT}
\end{equation} 

Recovery is due to a variety of possible mechanisms that include defect migration, interaction, annihilation, dissociation etc. All these are typically thermally activated and become effective at a particular temperature range. When the different mechanisms operate at distinct temperature intervals, one can observe well resolved peaks in the recovery rate spectrum. However, in many cases the peaks overlap making difficult to distinguish the various contributions. To obtain quantitative information the spectrum can be modelled as a weighted sum of uncorrelated recovery mechanisms
\begin{equation}
f(T) = \sum_l {A_l \, f_l(T)}
\label{rmodel}
\end{equation}
where $A_l$ measures the contribution of the $l$-th mechanism and $f_l(T)$ is the corresponding recovery function. $f_l(T)$ generally depends on the type of process and on a number of characteristic parameters as, e.g., the activation energy. 

As a first step towards analysing and interpreting a recovery spectrum it is helpful to decompose it according to eq. \eqref{rmodel} and obtain the values of $A_l$ and the kinetic parameters of the various processes. This can be achieved by least square fitting of (\ref{rmodel}) to the experimental data. In the following the relevant recovery laws for various types of mechanisms are described (section \ref{peakshapes}) and the fitting implementation in \texttt{MATLAB} is discussed (section \ref{s:fit}) and an example is given. 

\section{Recovery laws}
\label{peakshapes}

The recovery processes described here are thermally activated with a rate
\begin{equation}
k(T) = k_0 e^{-E / T}
\end{equation}
where $E$ is the activation energy and $k_0$ a pro-exponential factor. For simplicity it is used $k_B=1$. $k$ expresses the rate of successful events (e.g. atomic jumps, molecular dissociations, etc). 

It is useful to define the number of successful events up to the $i$-th annealing temperature
\begin{equation}
n(T_i) = \sum_{j=1}^i { k(T_i) \Delta t_i }.
\end{equation}

This can be generalized for continuously varying temperature profiles 
\begin{equation}
n(T) = \int_{0}^t k dt = \int_{T_0}^T k(T') \frac{dt}{dT'} dT' 
\end{equation}

In the following it is generally assumed that the fractional recovery $f$ is a function of $n$:
\[
f = f(n)
\]

\subsection{Linear heating}
Linear heating with a ramp rate $\alpha = dT/dt$ is a useful approximation that can be treated explicitly. The integration may be performed to give
\begin{equation}
n(T) = \frac{k_0 E}{\alpha} \int_{T_0/E}^{T / E} {e^{-1/x}dx} = \frac{k_0 E}{\alpha} 
\left[ F(T/E) - F(T_0/E)\right]
\label{equ::n} 
\end{equation}
where
\begin{equation}
F(z) = \int_0^z {e^{-1/x}dx} = z\, e^{-1/z} - \text{Ei}(1/z)
\end{equation}
and $\text{Ei}(z)=\int_z^\infty {dt \, e^{-t}/t}$ is the exponential integral (\texttt{MATLAB} function \texttt{expint}). A useful approximation of $F(z)$ is
\begin{equation}
F(z) \approx e^{-1/z} z^2 \left\lbrace 1 - 2z + 6z^2 + ... \right\rbrace 
\end{equation}
which is valid for $z \ll 1$. 

A usefull property of $n(T)$ is 
\begin{equation}
	\frac{(n')^2}{n''} = \frac{(dn/dT)^2}{d^2n/dT^2}  = \frac{k(T)}{\alpha} \frac{T^2}{E} 
\end{equation}
Using the expansion of $F(T/E)$ for $T/E \ll 1$ we obtain that
\begin{equation}
	(n')^2 / n'' \approx n
\end{equation}



\subsection{Maximum recovery rate}

In isochronal annealing experiments typically we observe peaks in the recovery rate at specific temperatures.

An analytical relation between such a peak temperature, $T_0$, and other physical parameters can be obtained from the condition at the maximum $d^2 f / dT^2=0$. For the linear heating experiment we obtain
\begin{equation}
	\frac{d^2 f}{dT^2} = \frac{d^2 n}{dT^2} \frac{df}{dn} + 
	\left(\frac{dn}{dT}\right)^2 \frac{d^2 f}{dn^2} = 0 
	\quad \Rightarrow \quad
	-\frac{df/dn}{d^2f/dn^2} = \frac{(dn/dT)^2}{d^2n/dT^2}  \approx n 
	\label{eq:max}
\end{equation}

If $\log T$ is considered as independent variable then a similar result is obtained 
\begin{equation}
	\frac{d^2 f}{d(\log T)^2} = 0 
	\quad \Rightarrow \quad
	-\frac{df/dn}{d^2f/dn^2} = 
	\frac{k(T)}{\alpha} \frac{T^2}{E}
	\left( 1+ \frac{T}{E} \right)^{-1}
	 \approx n 
\end{equation}

Further, it is interesting to seek the condition for maximum recovery rate as a function of $\log n$:
\begin{equation} \label{eq:maxlogn}
	\frac{d^2 f}{d(\log n)^2} = 0 
	\quad \Rightarrow \quad
	-\frac{df/dn}{d^2f/dn^2} = n 
\end{equation}
which is the same condition as eq. \eqref{eq:max} for linear heating. This is essentially due to the fact that a linear increment in $T$ corresponds roughly to a logarithmic increment of $n$.

\subsubsection{Correction to maximum recovery temperature}

It is noted that eq. \eqref{eq:maxlogn} is exact, whereas \eqref{eq:max} is only an approximation. In most cases the approximation is adequate and thus in a linear heating experiment, at the temperature of recovery rate maximum, $T=T_0$, eq. \eqref{eq:maxlogn} will be satisfied for $n=n_0=n(T_0)$. However, in the general case the temperature of the maximum will be at $T_0^* \neq T_0$. For a small deviation, $(T_0^* - T_0)/T_0 \ll 1$, we can obtain a correction to the peak temperature by the following procedure. We expand $f(\log n)$ near the recovery maximum
\[
f\approx f(\log n_0) + \left.\frac{df}{d \log n} \right|_{n_0} 
\left( \log n - \log n_0 \right)
 + \frac{1}{6} \left.\frac{d^3f}{d (\log n)^3} \right|_{n_0} 
\left( \log n - \log n_0 \right)^3
\]
where $n_0$ is the value of $n$ satisfying eq. \eqref{eq:maxlogn}. We also expand $\log n(T)$ around $T_0$, where $n(T_0) = n_0$:
\[
\log n \approx \log n_0 + \left.\frac{d\log n}{d T} \right|_{T_0} 
\left( T - T_0 \right)
+ \frac{1}{2} \left.\frac{d^2 \log n}{d T^2} \right|_{T_0} 
\left(T - T_0 \right)^2
\]
Substituting $\log n$ from the last equation to the above expansion of $f$ and requesting that $d^2f / dT^2 = 0$ at $T=T_0^*$ it is obtained that
\begin{equation}\label{eq:T0corr}
	\frac{T_0^*-T_0}{T_0} = - \frac{1}{T_0}
	\left. \frac{df/d\log n}{d^3 f/d\log n^3} \right|_{n_0}
	\left. \frac{d^2\log n / dT}{(d\log n / dT)^3} \right|_{T_0} \approx
	2 \left(\frac{T_0}{E}\right)^2
	\left. \frac{df/d\log n}{d^3 f/d\log n^3} \right|_{n_0}	.
\end{equation}


\subsection{1st Order process}

This is the simplest type of recovery process. The time evolution is given by
\begin{equation}
\frac{df}{dt} = - k(T) f.
\label{equ::1st}
\end{equation}
It can describe for example the spontaneous dissociation of isolated defects (e.g. close Frenkel Pairs). In that case $k(T)$ has the physical meaning of dissociation rate per defect. 

The general solution is
\begin{equation}
f = e^{-n} 
\end{equation}

The  peak position is found form \eqref{eq:maxlogn} and simplifies to
\begin{equation}
n_0=1 \quad \Rightarrow \quad
\left( \frac{E}{T_0}\right)^2 \exp \left( \frac{E}{T_0}\right) = \frac{k_0 \, E}{\alpha}
\label{equ::Tmax1st}
\end{equation}
Using Lambert's $W(z)$ function (solution of $z=W\,e^W$) the last relation may be transformed to
\begin{equation} \label{equ::Tmax1st2}
T_0=\frac{E}{2\, W(\sqrt{\frac{k_0 E}{4\alpha}})}
\end{equation}
or
\begin{equation}
E = T_0 W(k_0 T_0 / \alpha)
\end{equation}

\subsection{Higher Order Processes}

A $m$-th order process with $m>1$ is described by
\begin{equation}
\frac{df}{dt} = -k\, f^m
\end{equation}
For $m=2$ this equation can describe the recombination of two defects of equal concentration (e.g. I - V recombination). $k(T)$ represents then the interaction rate per defect pair.
Other values of $m$, even fractional, may describe more complex situations where several defects take part in the interaction.

The general solution is
\begin{equation}
f = \left[ 1 + (m-1) n \right]^{\frac{1}{1-m}} 
\end{equation}

The peak position is found from \eqref{eq:maxlogn} and simplifies again to
\begin{equation}
	n_0=1 
\end{equation}
thus the condition is the same as for the 1st-order process. 

\subsection{Correlated recombination of Frenkel pairs}

In this case the recovery after time $t$ can be written
\begin{equation}
	f(t) = 1-p(t)/p_\infty = 1 - \text{erfc}\left( \frac{r_i - r_0}{\sqrt{4 D t}} \right) 
\end{equation}
where $D=D_0 e^{-E/T}$ is the interstitial atom diffusion constant, $r_i$ the initial I-V separation and $r_0$ the I-V recombination radius. $p(t)$ is the probability that an interstitial recombines with its vacancy after time $t$, which for $t \to \infty$ is $p_\infty=r_0/r_i$ (Simpson \& Sossin 1970). 

Here, the number of events, $n$, is related to the diffusive jumps of the interstitial atom and thus is proportional to $D t$. We write $4 D t/(r_i - r_0)^2 = b\, n$, where $b$ is an arbitrary constant, and obtain
\begin{equation}
f= 1 - \text{erfc} \left( \frac{1}{\sqrt{b\, n}} \right).
\end{equation}

The peak position is found from \eqref{eq:maxlogn} and simplifies to
\[
	n_0=2/b 
\]
Setting $b=2$ we obtain again the condition $n_0=1$ at the peak and the temperature of the maximum for linear heating can be found from
\begin{equation}
	\left( \frac{E}{T_0}\right)^2 \exp \left( \frac{E}{T_0}\right) = 
	\frac{2 D_0 \, E}{\alpha (r_i-r_0)^2}
\end{equation}

\subsubsection{Exponential distribution of $r_i$}

According to Simpson \& Sossin (1970) if the initial distribution of interstitials relative to their vacancy is a modified Gaussian
\begin{equation}
g(r_i) = A \frac{e^{-r_i/\lambda r_0}}{r_i}
\end{equation}	 
then the recovery function becomes
\begin{equation}
f = e^{b\, n} \, \text{erfc}\sqrt{b\, n}; \quad b\, n = Dt/\lambda^2 r_0^2
\end{equation}
where $b$ is again an arbitrary constant.

From \eqref{eq:maxlogn} it is obtained
\begin{equation}
1+2 b\, n - 2 e^{b\, n}\sqrt{\pi b\, n} (1 + b\, n) \text{erfc} \sqrt{b\, n} = 0
\end{equation}
which can be solved numerically to get
\begin{equation}
b\, n_0 \approx 0.674573519 
\end{equation}
Setting $b=0.674573519$ we obtain again the condition $n_0=1$ at the peak and the temperature of the maximum for linear heating can be found from
\begin{equation}   
\left( \frac{E}{T_0}\right)^2 \exp \left( \frac{E}{T_0}\right) =  \frac{D_0 \, E}{\alpha\,  b\lambda^2 r_0^2}
\end{equation}

\section{Fitting procedure}
\label{s:fit}

\subsection{Unified recovery law definition}
\label{sec::peakshapeformulas}

To make the fitting procedure more intuitive and ease the selection of initial parameter values we rewrite the recovery laws as functions of $T$ with parameters $(T_0, k_0^*)$, where $T_0$ is the temperature of maximum recovery rate and $k_0^*$ is an effective rate constant. 

First we define the function 
\begin{equation}\label{eq:n2}
	n(T; T_0, k_0^*) =  k_0^* \sum_{T_i \leq T} {e^{-E(T_0,k_0^*)/T_i} \Delta t_i}
	= \frac{k_0^* E(T_0,k_0^*) }{\alpha} \int_0^{T/E(T_0,k_0^*)}{e^{-1/x}}dx
\end{equation}
where $E(T_0,k_0^*) = T_0 W(k_0^*\, T_0 / \alpha)$ is the corresponding activation energy as in eq. \eqref{equ::Tmax1st2} for the 1st order process. To evaluate $E(T_0,k_0^*)$ for stepwise isochronal annealing we estimate $\alpha\approx \Delta T/ \Delta t$ from temperature steps close to $T_0$ and use that to obtain $E$.

The recovery laws as a function of $T$ are written:
\begin{equation}
f(T;T_0,k_0^*) = f\left[ n(T; T_0, k_0^*) \right].
\end{equation} 
Each recovery law has a different definition for the effective rate $k_0^*$ as listed in Table \ref{tbl1}. The table summarizes also the $f(n)$ functions and the parameters
\[
q = - \left. \frac{df/d\log n}{d^3 f/d\log n^3} \right|_{n=1}
\]
entering eq. \eqref{eq:T0corr}. Thus, a correction to $T_0$ is calculated by
\begin{equation}\label{eq:Tmaxcorr}
(T_0^*-T_0)/ T_0 = - 2 q  (T_0/E)^2	
\end{equation}



\begin{table}[h]
\begin{center}
\begin{tabular}{lcccc}
\hline
\hline
Process & $k_0^*$ & $f(n)$ & $q$ & order code \\
\hline
1st Order & $k_0$ & $e^{-n}$ & 1 & 1 \\
Order $m>1$ & $k_0$ & $\left[ 1 + (m-1) n \right]^{\frac{1}{1-m}}$ & $m$ & $m$ \\
Corr. Recomb. - delta func. & $2D_0/(r_i-r_0)^2$ & $1 - \text{erfc}(1/\sqrt{2 n})$ & 2 & 0 \\
Corr. Recomb. - Exp. distr. & $D_0/b\lambda^2 r_0^2$ (*) & $e^{b\, n} \, \text{erfc}(\sqrt{b\, n})$ (*) & 6.145 & $1/2$\\
\hline
\hline
* $b\approx 0.674573519$
\end{tabular}
\caption{Definitions of effective rate parameter $k_0^*$, recovery function $f(n)$ and parameter $q$ of eq. \eqref{eq:Tmaxcorr} for the peak temperature correction. "Order code" is used in the \texttt{MATLAB} program. }
\end{center}
\label{tbl1}
\end{table}

\begin{figure}
	\includegraphics[width=0.5\textwidth]{f_vs_T}
	\includegraphics[width=0.5\textwidth]{df_vs_T}
	\caption{Recovery, $f$, (left) and recovery rate, $-df/dT$, (right) as a function of temperature for kinetic processes of order 1 ($m=1$) and 2 ($m=2$), correlated recombination with delta-function distribution ($m=0$) and modified exponential ($m=1/2$). $T_0$ corresponds to $n_0=n(T_0)=1$ where $d^2 f/(d\log n)^2|_{n_0}=0$. Corrections to the peak temperature obtained from eq. \eqref{eq:T0corr} are shown on the rate curves.}
\end{figure}

\subsection{Least squares fitting}

The experimental recovery is defined by a series of values 
\[
	\left\{ T_i, \Delta t_i, f^e_i  \right\}, \quad i=1,...,N
\]
It is fitted with a weighted sum of $L$ recovery mechanisms 
\[
	f^m_i = \sum_{l=1}^L{A_l f^m_{l,i}}.
\] 
where
\[
	f^m_{l,i} = f\left[n(T_i; T_0^l,k_0^l)\right]
\]
The best fit model parameters are found by minimizing 
\begin{equation}
\chi^2 = \sum_{i}{
	\left|
	\frac{\Delta f^e_i}{\Delta T_i} - \sum_l{ A_l \frac{\Delta f^m_{l,i}}{\Delta T_i} }
	\right|^2
}
\end{equation}

With respect to the weight parameters $A_l$ the minimization problem is linear and can be solved by matrix inversion. Thus the fitting procedure is done in 2 steps: A non-linear minimization method is used to obtain the best fit values for $T_0$ and $k_0$ while the $A_l$ are found in each step of the non-linear algorithm by a linear least squares minimization. 

\subsection{\texttt{MATLAB} implementation}

The fitting procedure is implemented in the function \texttt{fitrecov} 
\[
\texttt{[Pout,R,Rf,S]=fitrecov(Data,Par,options)}
\]
defined in the file \texttt{fitrecov.m}. It takes as input:
\begin{itemize}
	\item the experimental data in the form
	\[
	\texttt{Data = [T dt f]}
	\]
	where T, dt and f are column vectors of temperature, annealing time and recovery	
	\item An $L\times 5$ parameter matrix, \texttt{Par}, describing the $L$ peaks to fit the recovery. Each row has the form:
	\[
	\texttt{[T1 T2 log10(ko1) log10(ko2) c]}
	\]
	\texttt{T1, T2} define the temperature range to seek the recovery max \texttt{To}, i.e., \texttt{T1< To < T2}. Similarly \texttt{ko1, ko2} define the range to search for \texttt{ko}. Finally, \texttt{c} is the code number for the recovery law listed in \ref{tbl1}.
	\item The optional struct \texttt{options} contains variables that define (a) if differentiation of $f$ is with respect to $T$ (default), $\log T$ or $dt$ and (b) if plotting will be done in each iteration.
\end{itemize}

For the non-linear fitting the routine \texttt{lsqnonlin} is utilized. The fitting parameters are $\left\{ T_0, k_0 \right\}_l$, $l = 1, ..., L$, which are organized in a single vector \texttt{p}. The range for each parameter, upper and lower bound, are passed to  \texttt{lsqnonlin}.

The following sub-functions are defined in \texttt{fit\textunderscore rec\textunderscore peaks.m}:
\begin{description}
\item[\texttt{df = calc\textunderscore model(p)}] 
Calculates the model function based on the parameter vector \texttt{p}.
The weights $A_l$ that minimize $\chi^2$ are calculated with a call to \texttt{lsqnonneg}. This function ensures that the $A_l$ are all positive, since they represent peak weights. It returns the residuals $\delta f_i$.

\item[\texttt{df = chi2(p)}] 
Returns $\delta f_i$ so that $\chi^2 = \sum{\delta f_i^2}$. This function is passed to \texttt{lsqnonlin}.

\item[\texttt{f = rec\textunderscore func(n,c)}]
Calculates the recovery function $f(n)$ for the recovery law corresponding to code number $c$.

\item[\texttt{[n,E] = calc\textunderscore n(T,dt,To,ko)}] Calculates $n$ according to eq. \eqref{equ::n2}. Returns also $E(T_0,k_0)$.

\item[\texttt{Tc = corrTmax(To,E,m)}] Correct $T_0$ according to eq. \ref{eq:Tmaxcorr}.

\end{description}

\section{Examples}
There are also two example programs that show the usage and can be used for testing \texttt{fitrecov}:

\begin{description}
	\item[\texttt{test1\textunderscore fitrecov.m}] A recovery spectrum consisting of 2 peaks is first simulated and then fitted with \texttt{fitrecov} to obtain the parameters used in the simulation. 
	
	\item[\texttt{test2\textunderscore fitrecov.m}] An experimental recovery spectrum is fitted with \texttt{fitrecov} assuming 7 recovery peaks. 
\end{description} 

The output from the tests are shown in fig. \ref{fig:test1}. 


\begin{figure}
	%\centering
	\includegraphics[width=0.5\textwidth]{test1}
	\includegraphics[width=0.5\textwidth]{test2}
	\caption{The figures produced by \texttt{test1\textunderscore fitrecov.m} (left) and \texttt{test2\textunderscore fitrecov.m} (right)} \label{fig:test1}
\end{figure}

\end{document}