# fitrecov

MATLAB / OCTAVE function to fit isochronal recovery data

```matlab
% [Pout,R,Rf,S]=fitrecov(Data,Pin,options)
%
% Fit multiple recovery peaks to isochronal recovery data.
% Stepwise annealing is assumed.
```

Input arguments:
*  Data : [N x 3] data matrix
    *  1st column: temperature T (K)
    *  2nd column: annealing time dt (s)
    *  3rd column: recovery f (% or other units)
*  Par  : [L x 5] input parameter matrix for L peaks (each row is 1 peak)
    *  1st & 2nd column: (T1,T2) range to search for the peak max.
    *  3rd & 4th column: range for log10(ko), logarithm of the pro-exponential factor
    *  %           5th column: reaction code m
        *  m=0 : correlated recovery - delta function distribution
        *  m=0.5 : correlated recovery - mod. gaussian distribution
        *  m=1 : 1st order
        *  m>1 : m-th order
* options : [optional] options structure.
            Available options:
    * options.testing = {0} | 1. If set plotting is on during fitting and some messages are displayed
    * options.diff    = {'dT'} | 'dlogT' | 'dt' \\
    Select how to do the differentiation, df/dT, df/dlogT, df/dt

Output arguments
* Pout : [L x 4] output parameter matrix for L peaks (each row is 1 peak)
    * 1st column: Tmax peak temperature
    * 2nd column: A total peak recovery
    * 3rd column: ko, pre-exponential factor
    * 4th column: Ea, activation energy [e^(-Ea/kT)]
* R   : Nx1, experimental differential spectrum
* Rf  : Nx1, fitted differential spectrum
* S   : NxL, fitted individual peaks, scaled

![example](./examples/test2.png)

## Installation

Copy the file ``m/fitrecov.m`` to a folder in your MATLAB / OCTAVE path.

In OCTAVE you will need the ``optim`` package for ``lsqnonlin`` and the ``specfun`` package for ``lambertw``.

## Documentation

See the scripts in the ``examples/`` folder.

Detailed documentation in [fitrecov.pdf](./doc/fitrecov.pdf).


